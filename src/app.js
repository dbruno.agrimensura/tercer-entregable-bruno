const express = require('express');
const fs = require('fs');
const ProductManager = require('./ProductManager/productManager');

const app = express();
const port = 3000;

// Crear una instancia de ProductManager
const productManager = new ProductManager('ProductList.json');

// Ruta para obtener productos con opcionalidad de límite
app.get('/products',async (req, res) => {
    // Leer el parámetro de consulta 'limit'
    const limit = parseInt(req.query.limit);

    let products = await productManager.getProducts();

        res.json(products);
    });

    app.get('/products/:pid',async (req, res) => {
        const productId = req.params.pid;
    
        // Obtener el producto por su ID desde ProductManager
        const Id = parseInt(productId);
        const product = await productManager.getProductById(Id);
    
        if (product) {
            res.json(product);
        } else {
            res.status(404).send('Producto no encontrado');
        }
    });


app.listen(port, () => {
    console.log(`Servidor Express corriendo en http://localhost:${port}`);
});

// const express = require('express');
// const ProductManager = require('./productManager/productManager');

// const app = express();
// const port = 3000;

// // Crear una instancia de ProductManager
// const productManager = new ProductManager("./ProductList.json");

// // Ruta para obtener productos con opcionalidad de límite
// app.get('/products', (req, res) => {
//     // Leer el parámetro de consulta 'limit'
//     const limit = parseInt(req.query.limit);

//     // Obtener productos desde ProductManager
//     let products = productManager.getProducts();

//     // Aplicar el límite si se proporciona
//     if (!isNaN(limit)) {
//         products = products.slice(0, limit);
//     }

//     res.json(products);
// });

// app.listen(port, () => {
//     console.log(`Servidor Express corriendo en http://localhost:${port}`);
// });